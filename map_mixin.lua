local terrains = require 'terrains'

return {
	hasTerrain = function(self,x,y,name)
		local cell = self:get(x,y)
		if cell then
			local terrain = terrains[ cell.flags.terrain ]
			return terrain.name == name
		end
	end,
}