local Grid    = require 'Grid'
local MapCell = require 'MapCell'

local Map = Grid:extend 'Map' {
	objects = nil,
	width   = nil,
	height  = nil,
	
	init = function(self,width,height)
		
		Grid.init(self)
		
		self.objects      = {}
		self.width        = width
		self.height       = height
		self.flags        = {}
		
		for x = 0,width-1 do
			for y = 0,height-1 do
				self:set(x,y, MapCell())
			end
		end
	end,
	
	addObject = function(self, object)
		if self:hasObject(object) then error 'Map already has object!' end
		if object:getMap() then error 'Remove object from the other map before adding!' end
		object:_setMap(self)
		local x,y  = object:getPosition()
		table.insert(self.objects, object)
		table.insert(self:get(x,y).objects, object)
		
		return object
	end,
	
	removeObject = function(self, object)
		if not self:hasObject(object) then
			error( 'Map does not have object '..tostring(object) )
		end
		object:_setMap(nil)
		for i,obj in ipairs(self.objects) do
			if obj == object then
				table.remove(self.objects,i)
				break
			end
		end
		self:_removeFromCell(object, object:getPosition())
	end,
	
	hasObject = function(self, the_obj,x,y)
		if x and y then
			local cell = self:get(x,y)
			if cell then
				for i,object in ipairs(cell.objects) do
					if object == the_obj then return true end
				end
			end
		end
		return self == the_obj:getMap()
	end,
	
	setCellFlag = function(self, x,y, name,value)
		local cell      = self:get(x,y)
		cell.flags[name]= value
	end,
	
	getCellFlag = function(self, x,y, name)
		local cell = self:get(x,y)
		return cell and cell.flags[name]
	end,
	
	setFlag = function(self, name, value)
		self.flags[name] = value
	end,
	
	getFlag = function(self, name)
		return self.flags[name]
	end,
	
	updatePosition = function(self, object, new_x,new_y)
		self:_removeFromCell(object, object:getPosition())
		table.insert(self:get(new_x,new_y).objects, object)
		object:_setPosition(new_x,new_y)
	end,
	
	_removeFromCell = function(self, object, x,y)
		local cell = self:get(x,y) 
			
		for i,obj in ipairs(cell.objects) do
			if obj == object then 
				table.remove(cell.objects,i)
				return
			end
		end
		
		error( ('Map unable to find object at (%d,%d)'):format(x,y) )
	end
}

return Map