--[[
zlib License:

Copyright (c) 2014 Minh Ngo

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
--]]

local class = require 'class'

local Grid = class 'Grid' {
	__call = function(self,x,y)
		return self:get(x,y)
	end
}

function Grid:init()
	self.cells = {}
end
---------------------------------------------------------------------------------------------------
function Grid:get(x,y)
	return self.cells[x] and self.cells[x][y]
end
---------------------------------------------------------------------------------------------------
function Grid:set(x,y,v)
	self.cells[x]    = self.cells[x] or {}
	self.cells[x][y] = v
	return v
end
---------------------------------------------------------------------------------------------------
function Grid:rectangle(x,y,x2,y2,mode)
	mode          = mode or 'all'
	local skipNil = mode ~= 'all' and mode == 'skip'
	local xi,yi   = x-1,y
	return function()
		while true do
			xi = xi+1
			if yi > y2 then return end
			if xi > x2 then 
				yi = yi + 1; xi = x-1 
			else
				local v = Grid.get(self,xi,yi)
				if v or not skipNil then
					return xi,yi,v
				end
			end
		end
	end
end
---------------------------------------------------------------------------------------------------
function Grid:iterate()
	local cells,x,t,y,v = self.cells
	return function()
		repeat
			if not y then 
				x,t = next(cells,x)
			end
			if not t then return end
			y,v = next(t,y)
		until v
		return x,y,v
	end
end
---------------------------------------------------------------------------------------------------
return Grid