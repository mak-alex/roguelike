local util = require 'util'

local tileset = {
	image  = love.graphics.newImage 'images/code437.png',
	rows   = 16,
	columns= 16,
}

tileset.quads = util.imageToQuads(tileset.image,tileset.rows,tileset.columns)

return tileset