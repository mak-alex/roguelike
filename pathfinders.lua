local PathFinder = require 'PathFinder'
local gs         = require 'gamestate'
local terrains   = require 'terrains'

local t = {
	pawn = PathFinder(function(nx,ny, x,y, node)
		local state = gs:current()
		local cell  = state.map:get(nx,ny)
		return terrains[cell.flags.terrain].solid and 1 or 0
	end),
}

t.king  = pawn
t.queen = pawn

return t