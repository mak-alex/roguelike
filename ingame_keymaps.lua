local events = require 'events'
local state  = require 'gamestate'

-- movements
local right = function()
	local state = state.current()
	events:move(state.player,1,0)
end

local left = function()
	local state = state.current()
	events:move(state.player,-1,0)
end

local down = function()
	local state = state.current()
	events:move(state.player,0,1)
end

local up = function()
	local state = state.current()
	events:move(state.player,0,-1)
end

local upleft = function()
	local state = state.current()
	events:move(state.player,-1,-1)
end

local upright = function()
	local state = state.current()
	events:move(state.player,1,-1)
end

local downleft = function()
	local state = state.current()
	events:move(state.player,-1,1)
end

local downright = function()
	local state = state.current()
	events:move(state.player,1,1)
end

local idle = function()
	local state = state.current()
	events:idle(state.player)
end

local default = {
	right= right,
	left = left,
	down = down,
	up   = up,
	s    = idle,
	
	kp7  = upleft,
	kp8  = up,
	kp9  = upright,
	kp4  = left,
	kp6  = right,
	kp1  = downleft,
	kp2  = down,
	kp3  = downright,
	kp5  = idle,
}

local shift = {
	['.'] = function()
		events:exitFloor('down')
	end,
	[','] = function()
		events:exitFloor('up')
	end,
}

return {
	default = default,
	shift   = shift,
}