local Object     = require 'Object'
local pathfinders= require 'pathfinders'
local gs         = require 'gamestate'
local events     = require 'events'
local monsters   = require 'monsters'
local msg_history= require 'msg_history'
local messages   = require 'messages'

local fast_monster = {
	queen = true,
	bishop= true,
	rook  = true,
}

local Monster = Object:extend 'Monster' {
	type       = 'monster',
	pathfinder = pathfinders.pawn,
	last_seen_x= nil,
	last_seen_y= nil,
	path       = nil,
	path_index = 0,
	discovered = false,

	init = function(self,x,y, monster_index, map)
		Object.init(self,x,y,map)
		self.sight_radius= 5
		local monster    = monsters[monster_index]
		self.name        = monster.name
		self.character   = monster.character
		self.fg_color    = monster.color
		self.pathfinder  = pathfinders[self.name]
	end,
	update = function(self,delay)
		-- Move towards player
		local state  = gs:current()
		local player = state.player
		local x,y    = self:getPosition()
		local px,py  = player:getPosition()
		self:setTurnOver(true)
		-- monster is in range of player's vision
		-- which means it sees the player
		-- check if path needs updating
		if state.visible_grid(x,y) then
			if px ~= self.last_seen_x or py ~= self.last_seen_y then
				self.path = self.pathfinder:compute(x,y, px,py)
				if self.path then self.path_index = 2 end
				self.last_seen_x,self.last_seen_y = px,py
				
				-- monster was first discovered
				-- don't do anything
				if not self.discovered then
					self.discovered = true
					msg_history:write(messages[1]:format(self.name))
					return
				end
			end
		end
		
		-- move towards last seen position of player
		if self.path then
			local _,dx,dy = self:_move()
			local success
			if fast_monster[self.name] then
				while self.path(self.path_index) do
					local x,y     = self:getPosition()
					local x2,y2   = self.path(self.path_index)
					local dx2,dy2 = x2-x,y2-y
					
					if dx == dx2 and dy == dy2 then
						success,dx,dy = self:_move()
						if not success then break end
					else
						break
					end
				end
			end
		end	
	end,
	_move = function(self)
		local x2,y2 = self.path(self.path_index)
		if x2 then
			local x,y   = self:getPosition()
			local dx,dy = x2-x,y2-y
			events:move(self,dx,dy)
			local x,y = self:getPosition()
			if x == x2 and y == y2 then
				self.path_index = self.path_index + 1
				return true,dx,dy
			end
		end
	end,
}

return Monster