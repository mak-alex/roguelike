local Map      = require 'Map'
local map_mixin= require 'map_mixin'
local Monster  = require 'Monster'

return {
	newMap = function(...)
		local map = Map(...)
		map.last_seen_grid = nil
		map.last_player_x  = nil
		map.last_player_y  = nil
		map:mixin(map_mixin)
		return map
	end,
}