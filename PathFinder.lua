--[[
zlib License:

Copyright (c) 2014 Minh Ngo

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
--]]

-- Pathfinding via A*
-- A good explanation:
-- http://www.policyalmanac.org/games/aStarTutorial.htm

--[[
Priority queue must implement the following methods:

	:new()            = constructor
	:insert(key,value)= insert key/value into the list
	:pop()            = pop the first key/value in the list
	:delete(key,value)= remove the key/value
	:clear()          = clear the list
	:length()         = return the length of the list
]]
local priority_queue= require 'SkipList'
local Grid          = require 'Grid'
local class         = require 'class'

-- Chebyshev distance
local function default_heuristic(x,y, goal_x,goal_y)
	local dx = math.abs(goal_x-x)
	local dy = math.abs(goal_y-y)
	return math.max(dx,dy)
end

-- Neighbor offsets from origin
local neighbor_coords = {
	-- orthogonal
	{0,1},
	{0,-1},
	{1,0},
	{-1,0},
	
	-- diagonal
	{-1,1},
	{1,1},
	{-1,-1},
	{1,-1},
}

-- Return iterator for the 8 neighboring nodes
-- The iterator should return the coordinate offsets from the origin
-- for dx,dy in neighbors() do ... end
local function default_neighbors()
	local index = 0
	return function()
		index = index + 1
		local coords = neighbor_coords[index]
		if coords then return coords[1],coords[2] end
	end
end

------------------------------------------------------------------------
-- CALLBACK EXAMPLE
------------------------------------------------------------------------

--[[
Two callbacks are used to get the total cost of a cell. Read up on
A* to understand them. The current node is passed as a final argument to 
both callbacks.

COST TO GOAL    = heuristic(node_x,node_y, goal_x,goal_y, node)
	Used to determine the minimum cost of movement from the node to the 
	goal. The default heuristic uses Chebyshev distance.

COST FROM START = path_cost(neighbor_x,neighbor_y, node_x,node_y, node)
	Used to determine the cost of moving from a node to a neighboring 
	node.
	
for dx,dy in neighbors() do ... end
	Used to get the neighboring nodes for pathfinding. For example, one 
	can return diagonal neighbors only to prevent orthogonal movements.
	The coordinates returned are offsets relative to the current node.
]]

------------------------------------------------------------------------
-- NODE CLASS
------------------------------------------------------------------------

local Node = class 'Node' {
	init = function(self,x,y,parent,cost_start,cost_end)
		self.x,self.y   = x,y
		self.parent     = parent
		self.cost_start = cost_start or 0
		self.cost_end   = cost_end or 0
		self.total_cost = self.cost_start+self.cost_end
		self.closed     = false
	end,
}

------------------------------------------------------------------------
-- PATH CLASS
------------------------------------------------------------------------

local Path = class 'Path' {
	__call = function(self,...)
		return self:get(...)
	end,

	init = function(self,pathfinder, x_from,y_from, x_to,y_to)
		self.pathfinder = pathfinder
		self.x_from     = x_from
		self.y_from     = y_from
		self.x_to       = x_to
		self.y_to       = y_to
		self.open       = priority_queue:new()
		self.node_grid  = Grid()
		self.done       = nil
		self.found      = nil
		self.path       = nil
		self:clear()
	end,
	
	-- Clear all path data and restart search at the first node
	clear = function(self)
		self.open:clear()
		self.node_grid:init()
		
		self.done       = false
		self.found      = false
		self.path       = {}
		
		local x_from,y_from,x_to,y_to = self.x_from,self.y_from,self.x_to,self.y_to
		
		-- The first node
		local start_node      = Node(x_from,y_from)
		start_node.cost_end   = self.pathfinder.heuristic(x_from,y_from, x_to,y_to, start_node)
		start_node.total_cost = start_node.cost_start+start_node.cost_end
		self.open:insert(start_node.total_cost,start_node)
	end,
	
	-- Search and return one node at a time 
	-- until a path is found (or not)
	-- This is pretty much an iterator
	search = function(self)
		local _,node
		-- Search until no path is found
		if self.open:length() > 0 then
			-- Get the node with the lowest cost
			_,node       = self.open:pop()
			node.closed  = true
			
			local x_from,y_from = self.x_from,self.y_from
			local x_to,y_to     = self.x_to,self.y_to
			local pathfinder    = self.pathfinder
			
			-- Create path if the node is the destination
			if node.x == x_to and node.y == y_to then
				while node do
					table.insert(self.path,1,node)
					node = node.parent
				end
				self.done  = true
				self.found = true
				
				-- Clear open and closed data
				self.open:clear()
				self.node_grid:init()
			else
				for dx,dy in pathfinder.neighbors() do
					local nx,ny = node.x+dx,node.y+dy
					self:_checkNeighbor(nx,ny,node,x_to,y_to)
				end
			end
		else
			self.done = true
		end
		
		return node
	end,
	
	-- Return an iterator which searches and return one node a time
	-- Iterator quits when search is over
	searchIterate = function(self)
		return self.search,self
	end,
	
	-- Compute the entire path (or not)
	-- Return true if a path is found
	compute = function(self)
		local search = self.search
		while search(self) do
		end
		return self.found
	end,
	
	_walkIter = function(path,index)
		index     = index + 1
		local node= path[index]
		if node then return index,node.x,node.y end
	end,
	
	-- Return an iterator which walks the path (if one was found)
	-- The iterator returns index,x,y for each step
	walk = function(self,mode)
		if mode == 'reverse' then
			local iter = function(path,index)
				index     = index - 1
				local node= path[index]
				if node then return index,node.x,node.y end
			end
			return iter,self.path,#self.path+1
		end
		return self._walkIter,self.path,0
	end,
	
	-- pop the next node in the path
	pop = function(self)
		local node = table.remove(self.path,1)
		if node then return node.x,node.y end
	end,
	
	length = function(self)
		return #self.path
	end,
	
	get = function(self,index)
		local node = self.path[index]
		if node then return node.x,node.y end
	end,
	
	_checkNeighbor = function(self, nx,ny, current_node, x_to,y_to)
		local node      = current_node
		local nnode     = self.node_grid:get(nx,ny) 
		local pathfinder= self.pathfinder     
		if nnode then
			-- The neighboring node is open
			-- else ignore it
			if not nnode.closed then
				-- Compare the cost of moving to this neighbor from the start and
				-- moving to this neighbor by going through this node from the start
				-- If the new cost is cheaper, readjust neighbor's cost and
				-- parent to this node
				-- Update the open list as well
				local new_cost_start = pathfinder.path_cost(nx,ny, node.x,node.y,node) + node.cost_start
				if new_cost_start < nnode.cost_start then
					self.open:delete(nnode.total_cost,nnode)
					nnode.cost_start = new_cost_start
					nnode.total_cost = new_cost_start + nnode.cost_end
					nnode.parent     = node
					self.open:insert(nnode.total_cost,nnode)
				end
			end
		elseif pathfinder.path_cost(nx,ny, node.x,node.y,node) ~= math.huge then
			-- Ignore the neighbor if the path cost is infinite, else
			-- it is new, add it as a child of the current node
			-- and add it to the open list
			local cost_start = pathfinder.path_cost(nx,ny, node.x,node.y,node) + node.cost_start
			local cost_end   = pathfinder.heuristic(nx,ny, x_to,y_to, nnode)
			local nnode      = Node(nx,ny,node,cost_start,cost_end)
			self.node_grid:set(nx,ny,nnode)
			self.open:insert(nnode.total_cost,nnode)
		end
	end,
}

------------------------------------------------------------------------
-- PATHFINDER CLASS
------------------------------------------------------------------------

return class 'Pathfinder' {
	init = function(self, path_cost, heuristic, neighbors)
		self.path_cost= path_cost
		self.heuristic= heuristic or default_heuristic
		self.neighbors= neighbors or default_neighbors
	end,
	
	newPath = function(self, x_from,y_from, x_to,y_to)
		-- New path
		return Path(self, x_from,y_from, x_to,y_to)
	end,	
	
	-- Return a path if one was found.
	compute = function(self, x_from,y_from, x_to,y_to)
		local path = Path(self, x_from,y_from, x_to,y_to)
		if path:compute() then return path end
	end,	
}
