-- Based on the priority queue discussed here:
-- http://kooneiform.wordpress.com/2009/04/14/roguelike-turn-based-time/

local SkipList = require 'SkipList'

local Scheduler = class 'Scheduler' {
	init = function(self)
		self.queue        = SkipList:new()
		self.delays       = {}
		self.current_obj  = false
		self.current_delay= nil
	end,
	
	add = function(self, object, delay)
		if self.delays[object] then error 'Object already added!' end
		delay = delay or 0
		self.queue:insert(delay,object)
		self.delays[object] = delay
	end,
	
	-- Removes object and returns the associated delay
	-- Returns nothing if object was not found
	remove = function(self, object)
		local delay        = self.delays[object]
		self.delays[object]= nil
		return self.queue:delete(delay,object)
	end,
	
	-- getDelay = function(self,object)
		-- return self.delays[object]
	-- end,
	
	-- Pop the next object and delay
	-- Return false if empty
	getNext = function(self)
		local delay,object = self.queue:pop()
		self.current_obj   = object
		self.current_delay = delay
		self.delays[object]= nil
		return object,delay
	end,
	
	getCurrent = function(self)
		return self.current_obj,self.current_delay
	end,
}

return Scheduler
