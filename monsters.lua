local colors = require 'colors'

local monsters = {
	--------------------------
	-- List of monsters
	--------------------------
	[1] = {
		name = 'nothing', 
		character = '\0', 
		color = colors.white,
	},
	[2] = {
		name = 'player', 
		character = '\1', 
		color = colors.white,
	},
	[3] = {
		name = 'pawn', 
		character = 'p', 
		color = colors.red,
	},
	[4] = {
		name = 'queen', 
		character = 'Q', 
		color = colors.yellow,
	},
}

return monsters