local Object = class 'Object' {
	-- PUBLIC
	turn_cost    = 1,
	total_delay  = 0,
	sight_radius = 5,
	type         = nil,
	character    = nil,
	fg_color     = nil,
	bg_color     = nil,
	
	-- PRIVATE
	_x        = 0,
	_y        = 0,
	_map      = nil,
	_turn_over= false,
	
	init = function(self,x,y,map)
		if map then map:addObject(self) end
		self._x,self._y = x,y
	end,
	
	_setMap = function(self,map)
		self._map = map
	end,
	
	getMap = function(self)
		return self._map
	end,
	
	getPosition = function(self)
		return self._x,self._y
	end,
	
	getX = function(self)
		return self._x
	end,
	
	getY = function(self)
		return self._y
	end,
	
	move = function(self,dx,dy)
		self:setPosition(self._x+dx,self._y+dy)
	end,
	
	_setPosition = function(self,x,y)
		self._x,self._y = x,y
	end,
	
	setPosition = function(self,x,y)
		if self._map then self._map:updatePosition(self,x,y)
		else self:_setPosition(x,y) end
	end,
		
	setTurnOver = function(self,boolean)
		if boolean == nil then error 'Must pass true or false' end
		self._turn_over = boolean
	end,
	
	getTurnOver = function(self)
		return self._turn_over
	end,
	
	update = function(self,delay)
	end,	
}
return Object