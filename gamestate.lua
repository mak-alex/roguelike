local gamestate    = {}
local currentstate = gamestate

function gamestate.register()
	for _,callback in pairs{
		-- 'errhand',
		'focus',
		'keypressed',
		'keyreleased',
		'load',
		'mousefocus',
		'mousepressed',
		'mousereleased',
		'quit',
		'resize',
		'textinput',
		'threaderror',
		'visible',
		
		'gamepadaxis',
		'gamepadpressed',
		'gamepadreleased',
		'joystickadded',
		'joystickaxis',
		'joystickhat',
		'joystickpressed',
		'joystickreleased',
		'joystickremoved',
	} 
	do
		love[callback] = function(...)
			if currentstate[callback] then currentstate[callback](currentstate,...)
			elseif gamestate[callback] then gamestate[callback](currentstate,...) end
		end
	end
	
	love.update = function(...)
		if gamestate.preUpdate then gamestate.preUpdate(currentstate,...) end
		if currentstate.update then currentstate:update(...) end
		if gamestate.postUpdate then gamestate.postUpdate(currentstate,...) end
	end
	
	love.draw = function(...)
		if gamestate.preDraw then gamestate.preDraw(currentstate,...) end
		if currentstate.draw then currentstate:draw(...) end
		if gamestate.postDraw then gamestate.postDraw(currentstate,...) end
	end
end

function gamestate.switch(state)
	local oldstate = currentstate
	if oldstate['exit'] then oldstate['exit'](oldstate,state) end
	
	currentstate = state or gamestate
	
	if currentstate['enter'] then currentstate['enter'](currentstate,oldstate) end
end

function gamestate.current()
	return currentstate
end

return gamestate