-----------------------------
-- IMPORT SCRIPTS
-----------------------------
local tileset= require 'tileset'
local colors = require 'colors'

-----------------------------
-- MAP DISPLAY
-----------------------------
local WIDTH  = 40
local HEIGHT = 30

local display = {
	width   = nil,
	height  = nil,
	
	chars    = {},
	fg_colors= {},
	bg_colors= {},
	
	fg_batch= nil,
	bg_batch= nil,
	bg_image= nil,
	bg_quad = nil,	
	_update = nil,
}

display.init = function(self,width,height)
	self:clear()
	
	local _,_,qw,qh = tileset.quads[0]:getViewport()
	local bg_image  = love.image.newImageData(qw, qh)
		
	bg_image:mapPixel(function(x,y,r,g,b,a)
		return 255,255,255,255
	end)
	
	self.width,self.height = width,height
	self.bg_image = love.graphics.newImage(bg_image)
	self.bg_quad  = love.graphics.newQuad(0,0, qw,qh, qw,qh)
	
	self.fg_batch = love.graphics.newSpriteBatch(tileset.image, self.width*self.height)
	self.bg_batch = love.graphics.newSpriteBatch(self.bg_image, self.width*self.height)
	self._update  = true
end

display.getSize = function(self)
	return self.width,self.height
end

display.getRealSize = function(self)
	local _,_,qw,qh = self.bg_quad:getViewport()
	return self.width*qw,self.height*qh
end

display.setSize = function(self,width,height)
	self.width,self.height = width,height
	self.fg_batch = love.graphics.newSpriteBatch(tileset.image, self.width*self.height)
	self.bg_batch = love.graphics.newSpriteBatch(self.bg_image, self.width*self.height)
	self._update  = true
end

display.clear = function(self)
	self.chars    = {}
	self.fg_colors= {}
	self.bg_colors= {}
	self._update  = true
end

display._getIndex = function(self,x,y)
	return y*self.width + x
end

display.inRange = function(self,x,y)
	return x > -1 and x < self.width and y > -1 and y < self.height
end

display._rangeCheck = function(self,x,y)
	if not self:inRange(x,y) then 
		local ERROR_TEMPLATE = 'Coordinates are out of display range: (%d,%d)'
		error(string.format(ERROR_TEMPLATE,x,y))
	end
end

display.setCharacter = function(self, x,y, char)
	self:_rangeCheck(x,y)
	local index      = self:_getIndex(x,y)
	self.chars[index]= char or '\0'
	self._update     = true
	return self
end

local default_callback = function(new_r,new_g,new_b, old_r,old_g,old_b)
	return new_r,new_g,new_b
end

display.setBackground = function(self, x,y, color, callback)
	self:_rangeCheck(x,y)
	local bg_color,index,default
	
	index   = self:_getIndex(x,y)
	callback= color and callback or default_callback
	default = colors.background
	bg_color= self.bg_colors[index] or {default[1],default[2],default[3]}
	self.bg_colors[index] = bg_color
	self._update = true
	
	bg_color[1],bg_color[2],bg_color[3] = callback(
		color[1],color[2],color[3],
		bg_color[1],bg_color[2],bg_color[3]);
	
	return self
end

display.setForeground = function(self, x,y, color, callback)
	self:_rangeCheck(x,y)
	local fg_color,index,default
	
	index   = self:_getIndex(x,y)
	callback= color and callback or default_callback
	default = colors.foreground
	color   = color or default
	fg_color= self.fg_colors[index] or {default[1],default[2],default[3]}
	self.fg_colors[index] = fg_color
	self._update = true
	
	fg_color[1],fg_color[2],fg_color[3] = callback(
		color[1],color[2],color[3],
		fg_color[1],fg_color[2],fg_color[3]);
	
	return self
end

display.setCell = function(self, x,y, char, fg_color,bg_color)
	self:setCharacter(x,y, char)
	self:setForeground(x,y, fg_color)
	self:setBackground(x,y, bg_color)
	return self
end
	
display.update = function(self)
	local _,_,qw,qh = self.bg_quad:getViewport()

	local fgb = self.fg_batch
	local bgb = self.bg_batch
	
	fgb:clear()
	bgb:clear()
	bgb:bind()
	fgb:bind()
	
	for gx = 0,self.width-1 do
		local x = gx*qw
		for gy = 0,self.height-1 do
			local y    = gy*qh
			local i    = self:_getIndex(gx,gy)
			local code = (self.chars[i] or '\0'):byte()
			local quad = tileset.quads[code] or tileset.quads[0]
			
			bgb:setColor( unpack(self.bg_colors[i] or colors.background) )
			bgb:add(self.bg_quad, x,y)
			
			fgb:setColor( unpack(self.fg_colors[i] or colors.foreground) )
			fgb:add(quad, x,y)
		end
	end
	
	bgb:unbind()
	fgb:unbind()
	
	self._update = false
	
	return self
end

display.draw = function(self,...)
	if self._update then self:update() end

	love.graphics.draw(self.bg_batch,...)
	love.graphics.draw(self.fg_batch,...)
	return self
end

-----------------------------
-- INITIATE DISPLAY
-----------------------------

display:init(WIDTH,HEIGHT)

return display