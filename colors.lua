local util = require 'util'
local rgb  = util.colorHexToRGB

local colors =  {
	foreground = {rgb(0xdedede)},
	background = {rgb(0x2b2b2b)},

	-- normal
	white        = {rgb(0xd3d7cf)},
	black        = {rgb(0x2e3436)},
	red          = {rgb(0xCC0000)},
	green        = {rgb(0x4e9a06)},
	blue         = {rgb(0x3465a4)},
	cyan         = {rgb(0x06989a)},
	yellow       = {rgb(0xc4a000)},
	magenta      = {rgb(0x75507b)},
	
	-- light
	light_white  = {rgb(0xeeeeec)},
	light_black  = {rgb(0x555753)},
	light_red    = {rgb(0xef2929)},
	light_green  = {rgb(0x8ae234)},
	light_blue   = {rgb(0x728fcf)},
	light_cyan   = {rgb(0x34e2e2)},
	light_yellow = {rgb(0xfce94f)},
	light_magenta= {rgb(0xad7fa8)},
}

return colors