--[[
zlib License:

Copyright (c) 2014 Minh Ngo

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.
--]]

-- Simple class with inspiration from other class modules
-------------------------------------------------
local class,base

-- Syntax: class "name" {...}
class = function(name)
	return function(prototype)
		local class  = prototype or {}
		class.__class= name
		class.__index= class
		return setmetatable(class,base)
	end
end

base = {__class = 'Object'}
base.__index  = base

-- create class instance 
-- and initiate if necessary
function base.__call(class,...)
	local obj = setmetatable({},class.__proxy_meta or class)
	if obj.init then obj:init(...) end
	return obj
end

function base.new(class,...)
	return base.__call(class,...)
end

function base.type(obj)
	return obj.__class
end

function base.typeOf(obj,name)
	while obj do
		if obj.__class == name then return true end
		local meta = getmetatable(obj)
		obj = meta and meta.__index
	end
	return false
end

function base:super()
	return getmetatable(self).__index
end

local metamethods = {
	'add',
	'sub',
	'mul',
	'div',
	'mod',
	'pow',
	'unm',
	'concat',
	'len',
	'eq',
	'lt',
	'le',
	'call',
	'tostring',
} 

-- create subclass
function base:extend(name)
	return function(prototype)
		local super   = self
		local class   = class(name) (prototype)
		class.__proxy_meta = {__index = class}
		
		local pmeta = class.__proxy_meta
		
		for _,metamethod in ipairs(metamethods) do
			metamethod        = '__'..metamethod
			pmeta[metamethod] = function(...)
				return class[metamethod](...)
			end
		end
		
		return setmetatable(class,{__index = super, __call = base.__call})
	end
end

local reserved = {
	__index     = true,
	__class     = true,
	__proxy_meta= true,
	__mixin_list= true,
}

-- Copy mixin source
-- Use some magic to use more than one duplicate mixin functions
-- Call source:_mixin(obj,...) at the end
function base:mixin(source,...)
	self.__mixin_list = self.__mixin_list or {}
	for k,v in pairs(source) do
		local v_type = type(v)
		if not reserved[k] then 
			if v_type == 'function' then
				local list           = self.__mixin_list[k]
				self.__mixin_list[k] = list or {}
				table.insert(self.__mixin_list[k],v)
				if not list then 
					self[k] = function(self,...)
						local list = self.__mixin_list[k]
						local len  = #list
						for i = 1,len-1 do
							list[i](self,...)
						end
						return list[len](self,...)
					end
				end
			else
				self[k] = v 
			end
		end
	end
	if source._mixin then source:_mixin(self,...) end
	return self
end

-- http://lua-users.org/wiki/CopyTable
local function deep_copy(t,done)
	if done[t] then return done[t]
	elseif type(t) ~= 'table' then return t end
	
	local newt = setmetatable( {}, getmetatable(t) )
	done[t]    = newt
	for i,v in pairs(t) do
		i      = deep_copy(i,done)
		v      = deep_copy(v,done)
		newt[i]= v
	end
	return newt
end

function base:clone()
	return deep_copy(self,{})
end

-- interface for cross class-system compatibility (see https://github.com/bartbes/class-Commons).
if class_commons ~= false and not common then
	common = {}
	function common.class(name, prototype, super)
		return super and super:extend( name ) (prototype) or class( name ) (prototype)
	end
	function common.instance(class, ...)
		return class(...)
	end
end

return class