local Object = require 'Object'

local Player = Object:extend 'Player' {
	init = function(self,x,y,map)
		Object.init(self,x,y,map)
		self.character   = '\1'
		self.name        = 'player'
		self.sight_radius= 8
	end,
}

return Player