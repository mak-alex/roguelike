local Map          = require 'Map'
local Object       = require 'Object'
local Scheduler    = require 'Scheduler'
local display      = require 'display'
local terrains     = require 'terrains'
local tileset      = require 'tileset'
local input        = require 'input'
local colors       = require 'colors'
local keymaps      = require 'ingame_keymaps'
local Grid         = require 'Grid'
local fov          = require 'fov'
local map_generator= require 'map_generator'
local Player       = require 'Player'
local Monster      = require 'Monster'
local gamestate    = require 'gamestate'
local factories    = require 'factories'
local msg_history  = require 'msg_history'

local ingame = {
	enter = function(self,oldstate)
		self.map_list      = {}
		self.current_floor = 0 -- 1 is the first floor
		self.player        = Player(1,1)
		self.scheduler     = Scheduler()
		self.visible_grid  = Grid()
		self.last_seen_grid= nil
		input.default_map  = keymaps.default
		input.shift_map    = keymaps.shift
		
		self:generateFloor()
	end,
	
	mousepressed = function(self,x,y,b)
	end,
	
	keypressed = function(self,k)
		if self.scheduler:getCurrent() == self.player then input:keypressed(k) end
	end,
	
	keyreleased = function(self,k)
		input:keyreleased(k)
	end,
	
	textinput = function(self,text)
		input:textinput(text)
	end,
	
	update = function(self,dt)
		input:update(dt)
		
		local scheduler = self.scheduler
		local curr_obj,curr_delay = scheduler:getCurrent()
		if not curr_obj or curr_obj:getTurnOver() then
			if curr_obj == self.player then 
				self:_updatePlayerSight() 
			end
			-- ERROR (LAST MESSAGE)
			if curr_obj then scheduler:add(curr_obj,curr_delay + curr_obj.turn_cost) end
			local obj,delay = scheduler:getNext()
			if obj == self.player then 
				self:_updatePlayerSight() 
				self:_updateDisplay()	
			end
			obj:setTurnOver(false)
			obj:update(delay)
		end
	end,
	
	draw = function(self)
		display:draw()
		love.graphics.push()
			local w,h = display:getRealSize()
			local font= love.graphics.getFont()
			local fh  = font:getHeight()
			love.graphics.translate(0,h)
			for index,line in msg_history:iterate() do
				-- print(line)
				love.graphics.print(line,0,0)
				love.graphics.translate(0,fh)
			end
		love.graphics.pop()
		
		love.graphics.print('Current floor: '..self.current_floor,0,0)
	end,
	
	exit = function(self,newstate)
	end,
	
	setVisible = function(self,x,y,boolean)
		self.visible_grid:set(x,y,boolean)
	end,
	
	getVisible = function(self,x,y) return self.visible_grid(x,y) or false end,
	
	clearVisible = function(self)
		self.visible_grid = Grid()
	end,
	
	getOpaque = function(self,x,y)
		return self.map:getCellFlag(x,y,'opaque')
	end,
	
	destroy = function(self,object)
		self.map:removeObject(object)
		self.scheduler:remove(object)
	end,
	
	generateFloor = function(self,direction)
		direction = direction or 'up'
		if direction == 'up' then 
			self.current_floor = self.current_floor + 1
			if not self.map_list[ self.current_floor] then
				self.map = map_generator(self)
				table.insert(self.map_list,self.current_floor,self.map)
				self:_generateFlags()
				self:_generateObjects(true)
			-- Reuse previous floors
			else
				self.map = self.map_list[self.current_floor]
				self:_generateObjects()
			end
		elseif direction == 'down' then
			self.current_floor= self.current_floor - 1
			self.map          = self.map_list[self.current_floor]
			self:_generateObjects()
		end
		self.last_seen_grid = self.map.last_seen_grid or Grid()
	end,
	
	-- Generate objects and add to scheduler
	_generateObjects = function(self,is_new_floor)
		self.scheduler:init()
		if is_new_floor then
			local coords = self.map:getFlag(5)
			if coords then self.player:setPosition(coords[1],coords[2]) else
				-- first floor random player placement
				repeat
					local x,y = math.random(0,self.map.width-1),math.random(0,self.map.height-1)
					local found_floor = self.map:getCellFlag(x,y,'terrain') == 3
					if found_floor then
						self.player:setPosition(x,y)
					end
				until found_floor
			end
		end
		self.map:addObject(self.player)
		if self.map.last_player_x then
			self.player:setPosition(self.map.last_player_x,self.map.last_player_y)
		end
		-- Spawn new objects if it is a new floor
		if is_new_floor then
			local enemy = Monster(15,6, 4)
			self.map:addObject(enemy)
		end
		-- Add to scheduler
		for i,object in pairs(self.map.objects) do
			self.scheduler:add(object,object.total_delay)
		end
	end,
	
	_generateFlags = function(self)
		-- Generate stairs up
		repeat
			local x,y = math.random(0,self.map.width-1),math.random(0,self.map.height-1)
			local found_floor = self.map:getCellFlag(x,y,'terrain') == 3
			if found_floor then
				self.map:setCellFlag(x,y, 'terrain',4)
				self.map:setFlag(4, {x,y})
			end
		until found_floor
		
		-- Generate stairs down
		if self.current_floor ~= 1 then
			repeat
				local x,y = math.random(0,self.map.width-1),math.random(0,self.map.height-1)
				local found_floor = self.map:getCellFlag(x,y,'terrain') == 3
				if found_floor then
					self.map:setCellFlag(x,y, 'terrain',5)
					self.map:setFlag(5, {x,y})
				end
			until found_floor
		end
	end,
	
	_updatePlayerSight = function(self)
		local player = self.player
		local px,py  = player:getPosition()
	
		self:clearVisible()
		fov(px,py,player.sight_radius,
			function(x,y) return not self:getOpaque(x,y) end,
			function(x,y)
				local radius = player.sight_radius
				local dx,dy  = x-player:getX(),y-player:getY()
				if (dx*dx + dy*dy) > radius*radius + radius then 
					return 
				end
			
				self:setVisible(x,y,true)
				self.map:setCellFlag(x,y,'explored',true)
				local cell = self.map(x,y)
				-- Field of view updates what was last seen
				if next(cell.objects) then
					self.last_seen_grid:set(x,y,cell.objects[#cell.objects].character)
				else
					self.last_seen_grid:set(x,y,terrains[cell.flags.terrain].character)
				end
			end
		)
		self.map:setCellFlag(px,py,'explored',true)
	end,
	
	_updateDisplay = function(self)
		
		-- Calculations to center the player on display
		local px,py   = self.player:getPosition()
		local hw,hh   = math.floor( display.width/2 ), math.floor( display.height/2 )
		local x1,y1   = px-hw,py-hh
		local x2,y2   = x1 + display.width-1, y1 + display.height-1
		
		display:clear()
				
		-- Update display
		for x,y,cell in self.map:rectangle(x1,y1, x2,y2, 'skip') do
			-- prioritize drawing objects
			local char,fg,bg
			local dx,dy = x-x1,y-y1
			if not DEBUG_REVEAL_ALL and not self:getVisible(x,y) then
				-- Draw the last seen object in shadow
				char = self.last_seen_grid(x,y) or '\0'
				if not self.map:getCellFlag(x,y,'explored') then
					fg,bg = colors.background,colors.background
				else
					fg,bg = colors.light_black,colors.background 
				end
			elseif next(cell.objects) then
				-- The last object is drawn ontop
				local obj   = cell.objects[#cell.objects]
				char  = obj.character
				fg    = obj.fg_color
				bg    = obj.bg_color
			elseif cell.flags.terrain then
				-- draw terrain
				local terrain = terrains[ cell.flags.terrain ]
				char    = terrain.character 
				fg      = terrain.foreground
				bg      = terrain.background
			end
			display:setCharacter(dx,dy, char)
			display:setForeground(dx,dy, fg)
			if bg then display:setBackground(dx,dy, bg) end
		end
		
		-- Draw player on top
		local dx,dy = px-x1,py-y1
		display:setCharacter(dx,dy, self.player.character)
		display:setForeground(dx,dy, self.player.fg_color)
		if bg then display:setBackground(dx,dy, self.player.bg_color) end
	end,
	
}

return ingame