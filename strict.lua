global = function(name)
	rawset(_G,name,false)
end

setmetatable(_G,{
	__newindex = function(t,k,v)
		error('Must declare '..tostring(k)..' as a global variable')
	end,
})